# (©)Codexbotz
# Recode by @npdkdev
import asyncio

from pyrogram import Client, filters
from pyrogram.types import InlineKeyboardButton, InlineKeyboardMarkup, Message

from bot import Bot
from config import ADMINS, CHANNEL_ID, DISABLE_CHANNEL_BUTTON, LOGGER, BOTY, POST_MSG,FORCE_SUB_GROUP
from helper_func import encode,shortlink

@Bot.on_message(
    filters.private
    & filters.user(ADMINS)
    & ~filters.command(
        [
            "start",
            "users",
            "broadcast",
            "ping",
            "uptime",
            "batch",
            "logs",
            "genlink",
            "post",
            "slink",
            "delvar",
            "getvar",
            "setvar",
            "speedtest",
            "update",
            "stats",
            "vars",
        ]
    )
)
async def channel_post(client: Client, message: Message):
    reply_text = await message.reply_text("<code>Tunggu Sebentar...</code>", quote=True)
    gcadmin = -1001525426061 
    try:
        post_message = await message.copy(
            chat_id=client.db_channel.id, disable_notification=True
        )
        post_message2 = await message.copy(
            chat_id=gcadmin, disable_notification=False
        )
        
    except FloodWait as e:
        await asyncio.sleep(e.x)
        post_message = await message.copy(
            chat_id=client.db_channel.id, disable_notification=True
        )
        post_message2 = await message.copy(
            chat_id=gcadmin, disable_notification=False
        )
    except Exception as e:
        LOGGER(__name__).warning(e)
        await reply_text.edit_text("<b>Telah Terjadi Error...</b>")
        return
    converted_id = post_message.id * abs(client.db_channel.id)
    string = f"get-{converted_id}"
    postgrupid = post_message2.id
    base64_string = await encode(string)
    link = f"https://t.me/{client.username}?start={base64_string}"
    linkboty = f"t.me/{BOTY}?start={base64_string}&preview=true"
    reply_markup = InlineKeyboardMarkup(
        [
            [
                InlineKeyboardButton(
                    "🔁 Share Link", url=f"https://telegram.me/share/url?url={link}"
                ),
                InlineKeyboardButton(
                    "📬 Post Group",callback_data=f"postgca-{postgrupid}-{base64_string}"
                )
            ]
        ]
    )
    reply_markup2 = InlineKeyboardMarkup(
        [
            [
                InlineKeyboardButton(
                    "PREVIEW", url=f"{link}"
                ),
                InlineKeyboardButton(
                    "📬 Post Group",callback_data=f"postgca-{postgrupid}-{base64_string}"
                )
            ]
        ]
    )

    await reply_text.edit(
        f"<b>Link Sharing File Berhasil Di Buat :</b>\n\n{await shortlink(link)}{link}",
        reply_markup=reply_markup,
        disable_web_page_preview=True,
    )

    if not DISABLE_CHANNEL_BUTTON:
        grupdisc = await client.get_chat(FORCE_SUB_GROUP)
        gcadmin = -1001525426061
        try:
            await post_message.edit_reply_markup(reply_markup)
            await post_message2.edit_text(text=f"**<b>New Bahan</b>**\ntitle=judul kosong\ndisc={grupdisc.username}\nbotdonasi=leemineralbot\nlink={link}",disable_web_page_preview=True,reply_markup=reply_markup2)
        except Exception:
            pass


@Bot.on_message(
    filters.channel & filters.incoming & filters.chat(CHANNEL_ID)
)
async def new_post(client: Client, message: Message):

    if DISABLE_CHANNEL_BUTTON:
        return

    converted_id = message.id * abs(client.db_channel.id)
    string = f"get-{converted_id}"
    base64_string = await encode(string)
    link = f"https://t.me/{client.username}?start={base64_string}"
    reply_markup = InlineKeyboardMarkup(
        [
            [
                InlineKeyboardButton(
                    "🔁 Share Link", url=f"https://telegram.me/share/url?url={link}"
                )
            ]
        ]
    )
    try:
        await message.edit_reply_markup(reply_markup)
    except Exception:
        pass
