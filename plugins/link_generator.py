# Recode @npdkdev

from pyrogram import Client, filters
from pyrogram.types import InlineKeyboardButton, InlineKeyboardMarkup, Message

from bot import Bot
from config import ADMINS, FORCE_SUB_GROUP
from helper_func import encode, get_message_id,shortlink

@Bot.on_message(filters.private & filters.user(ADMINS) & filters.command("post"))
async def postTo(client, message):
    try:
        grupdisc = await client.get_chat(FORCE_SUB_GROUP)
        gcadmin = -1001525426061
        _,link = message.text.split(" ",1)
        reply_markup2 = InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton(
                        "PREVIEW", url=f"{link}"
                    ),
                    InlineKeyboardButton(
                        "📬 Post Group",callback_data=f"postgca-"
                    )
                ]
            ]
        )
        csm = await client.send_message(
            chat_id=gcadmin,
            text=f"""**<b>New Bahan</b>**\ntitle=judul kosong\ndisc={grupdisc.username}\nbotdonasi=leemineralbot\nlink={link}""",
            disable_web_page_preview=True,
            reply_markup=reply_markup2
        )
        if csm:
            await message.reply_text("<code>Has been post at grup</code>")
    except Exception as e:
        await message.reply_text("Format salah")


@Bot.on_message(filters.private & filters.user(ADMINS) & filters.command("batch"))
async def batch(client: Client, message: Message):
    grupdisc = await client.get_chat(FORCE_SUB_GROUP)
    gcadmin = -1001525426061
    post_message2 = await message.copy(
        chat_id=gcadmin, disable_notification=False
    )
    while True:
        try:

            first_message = await client.ask(
                text="<b>Silahkan Forward Pesan/File Pertama dari Channel DataBase. (Forward with Qoute)</b>\n\n<b>atau Kirim Link Postingan dari Channel Database</b>",
                chat_id=message.from_user.id,
                filters=(filters.forwarded | (filters.text & ~filters.forwarded)),
                timeout=60,
            )
        except BaseException:
            return
        f_msg_id = await get_message_id(client, first_message)
        if f_msg_id:
            break
        await first_message.reply(
            "❌ <b>ERROR</b>\n\n<b>Postingan yang Diforward ini bukan dari Channel Database saya</b>",
            quote=True,
        )
        continue

    while True:
        try:
            second_message = await client.ask(
                text="<b>Silahkan Forward Pesan/File Terakhir dari Channel DataBase. (Forward with Qoute)</b>\n\n<b>atau Kirim Link Postingan dari Channel Database</b>",
                chat_id=message.from_user.id,
                filters=(filters.forwarded | (filters.text & ~filters.forwarded)),
                timeout=60,
            )
        except BaseException:
            return
        s_msg_id = await get_message_id(client, second_message)
        if s_msg_id:
            break
        await second_message.reply(
            "❌ <b>ERROR</b>\n\n<b>Postingan yang Diforward ini bukan dari Channel Database saya</b>",
            quote=True,
        )
        continue

    string = f"get-{f_msg_id * abs(client.db_channel.id)}-{s_msg_id * abs(client.db_channel.id)}"
    base64_string = await encode(string)
    postgrupid = post_message2.id
    link = f"https://t.me/{client.username}?start={base64_string}"
    reply_markup = InlineKeyboardMarkup(
        [
            [
                InlineKeyboardButton(
                    "🔁 Share Link", url=f"https://telegram.me/share/url?url={link}"
                ),
                InlineKeyboardButton(
                    "📬 Post Group", url=f"postgca-{postgrupid}-{base64_string}"
                )
            ]
        ]
    )
    reply_markup2 = InlineKeyboardMarkup(
        [
            [
                InlineKeyboardButton(
                    "PREVIEW", url=f"{link}"
                ),
                InlineKeyboardButton(
                    "📬 Post Group",callback_data=f"postgca-{postgrupid}-{base64_string}"
                )
            ]
        ]
    )
    await second_message.reply_text(
        f"<b>Link Sharing File Berhasil Di Buat:</b>\n\n{link}",
        quote=True,
        reply_markup=reply_markup,
    )
    await post_message2.edit_text(text=f"**<b>New Bahan</b>**\ntitle=judul kosong\ndisc={grupdisc.username}\nbotdonasi=leemineralbot\nlink={link}",disable_web_page_preview=True,reply_markup=reply_markup2) 


@Bot.on_message(filters.private & filters.user(ADMINS) & filters.command("genlink"))
async def link_generator(client: Client, message: Message):
    print("kontol")
    while True:
        try:
            channel_message = await client.ask(
                text="<b>Silahkan Forward Pesan dari Channel DataBase. (Forward with Qoute)</b>\n\n<b>atau Kirim Link Postingan dari Channel Database</b>",
                chat_id=message.from_user.id,
                filters=(filters.forwarded | (filters.text & ~filters.forwarded)),
                timeout=60,
            )
        except BaseException as e:
            print(e)
        msg_id = await get_message_id(client, channel_message)
        if msg_id:
            break
        await channel_message.reply(
            "❌ <b>ERROR</b>\n\n<b>Postingan yang Diforward ini bukan dari Channel Database saya</b>",
            quote=True,
        )
        continue

    base64_string = await encode(f"get-{msg_id * abs(client.db_channel.id)}")
    link = f"https://t.me/{client.username}?start={base64_string}"
    reply_markup = InlineKeyboardMarkup(
        [
            [
                InlineKeyboardButton(
                    "🔁 Share Link", url=f"https://telegram.me/share/url?url={link}"
                )
            ]
        ]
    )
    await channel_message.reply_text(
        f"<b>Link Sharing File Berhasil Di Buat:</b>\n\n{link}",
        quote=True,
        reply_markup=reply_markup,
    )

@Bot.on_message(filters.private & filters.user(ADMINS) & filters.command("slink"))
async def slink_generator(client: Client, message: Message):
    try:
        _,link = message.text.split(" ",1)
        slink = await shortlink(link)
        if slink:
            await message.reply_text(
                f"<b>ShortLink Sharing File Berhasil Di Buat:</b>\n\n{slink}",
                quote=True
            )
        else:
            await message.reply_text("ShortLink is Disable",True)
    except Exception as e:
        print(e)
