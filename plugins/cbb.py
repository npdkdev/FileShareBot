# (©)Codexbotz
# Recode by @npdkdev

from asyncio import exceptions
import asyncio
from pyrogram import Client


from bot import Bot
from main import boty
from config import API_HASH, APP_ID, OWNER,POST_MSG,FORCE_SUB_CHANNEL, TG_BOT_TOKEN2
from Data import Data
from pyrogram import enums, filters
from pyrogram.errors import FloodWait, MessageNotModified
from pyrogram.types import CallbackQuery, InlineKeyboardMarkup, Message


@Bot.on_message(filters.private & filters.incoming & filters.command("about"))
async def _about(client: Bot, msg: Message):
    await client.send_message(
        msg.chat.id,
        Data.ABOUT.format(client.username, OWNER),
        disable_web_page_preview=True,
        reply_markup=InlineKeyboardMarkup(Data.mbuttons),
    )


@Bot.on_message(filters.private & filters.incoming & filters.command("help"))
async def _help(client: Bot, msg: Message):
    await client.send_message(
        msg.chat.id,
        "<b>Cara Menggunakan Bot ini</b>\n" + Data.HELP,
        reply_markup=InlineKeyboardMarkup(Data.buttons),
    )


@Bot.on_callback_query()
async def cb_handler(client: Bot, query: CallbackQuery):
    data = query.data 
    if "postgca" in data:
        txt = query.message.caption if query.message.caption else query.message.text
        if not "judul kosong" in txt and query.message.media:
            await query.answer()
            try:
                msg = query.message
                fmt = txt.split("\n")
                _,fjudul = fmt[1].split("=",1)
                _,fdisc = fmt[2].split("=",1)
                _,fbot = fmt[3].split("=",1)
                _,flink = fmt[4].split("=",1)
                caps = ""
                if msg.caption:
                    caps = POST_MSG.format(judul=fjudul,botdonasi=fbot,unamedisc=fdisc,link=flink)
                match msg.media:
                    case enums.MessageMediaType.PHOTO:
                        thumb = msg.photo.file_id
                        await client.send_photo(
                            chat_id=int(FORCE_SUB_CHANNEL),
                            photo=thumb,
                            caption=caps
                        )
                    case enums.MessageMediaType.ANIMATION:
                        thumb = msg.animation.file_id
                        await client.send_animation(
                            chat_id=int(FORCE_SUB_CHANNEL),
                            animation=thumb,
                            caption=caps
                        )
                    case enums.MessageMediaType.VIDEO:
                        thumb = msg.video.file_id
                        await client.send_video(
                            chat_id=int(FORCE_SUB_CHANNEL),
                            video=thumb,
                            caption=caps
                        )

                await client.send_message(
                            chat_id=int(-1001525426061),
                            text="<code>Berhasil diposting</code>"
                        )
            except FloodWait as e:
                await asyncio.sleep(e.x)
                msg = query.message
                match msg.media:
                    case enums.MessageMediaType.PHOTO:
                        thumb = msg.photo.file_id
                        await client.send_photo(
                            chat_id=int(FORCE_SUB_CHANNEL),
                            photo=thumb,
                            caption=txt
                        )
                    case enums.MessageMediaType.ANIMATION:
                        thumb = msg.animation.file_id
                        await client.send_animation(
                            chat_id=int(FORCE_SUB_CHANNEL),
                            animation=thumb,
                            caption=txt
                        )
                    case enums.MessageMediaType.VIDEO:
                        thumb = msg.video.file_id
                        await client.send_video(
                            chat_id=int(FORCE_SUB_CHANNEL),
                            video=thumb,
                            caption=txt
                        )
            except ValueError:
                await query.message.reply_text("<code>Terjadi error</code>")
        else:
            try:
                await query.answer("Media masih kosong silahkan kirim gambar berserta judul")
                isMedia = True if query.message.media else False
                
                judulimg = await client.ask(
                    text="<b>Silahkan balas dengan image beserta judul</b>",
                    chat_id=query.message.chat.id,
                    filters=(filters.caption & (filters.photo | filters.animation | filters.video)),
                    timeout=60,
                )
                if judulimg:
                    thumb = None
                    judul = ""
                    
                    match judulimg.media:
                        case enums.MessageMediaType.PHOTO:
                            thumb = judulimg.photo.file_id
                            await client.send_photo(
                                chat_id=query.message.chat.id,
                                photo=thumb,
                                caption=f"{txt.replace('judul kosong',judulimg.caption)}",
                                reply_markup=query.message.reply_markup
                            )
                        case enums.MessageMediaType.ANIMATION:
                            thumb = judulimg.animation.file_id
                            await client.send_animation(
                                chat_id=query.message.chat.id,
                                animation=thumb,
                                caption=f"{txt.replace('judul kosong',judulimg.caption)}",
                                reply_markup=query.message.reply_markup
                            )
                        case enums.MessageMediaType.VIDEO:
                            thumb = judulimg.video.file_id
                            await client.send_video(
                                chat_id=query.message.chat.id,
                                video=thumb,
                                caption=f"{txt.replace('judul kosong',judulimg.caption)}",
                                reply_markup=query.message.reply_markup
                            )
                        
                
            except exceptions.TimeoutError:
                await client.send_message(query.message.chat.id,text="Waktu telah habis silahkan ulangi")
    elif data == "about":
        try:
            await query.message.edit_text(
                text=Data.ABOUT.format(client.username, OWNER),
                disable_web_page_preview=True,
                reply_markup=InlineKeyboardMarkup(Data.mbuttons),
            )
        except MessageNotModified:
            pass
    elif data == "help":
        try:
            await query.message.edit_text(
                text="<b>Cara Menggunakan Bot ini</b>\n" + Data.HELP,
                disable_web_page_preview=True,
                reply_markup=InlineKeyboardMarkup(Data.buttons),
            )
        except MessageNotModified:
            pass
    elif data == "close":
        await query.message.delete()
        try:
            await query.message.reply_to_message.delete()
        except BaseException:
            pass
