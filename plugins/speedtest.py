import os,sys

import speedtest
from pyrogram import filters
from pyrogram.types import Message
import math
from bot import Bot
from config import ADMINS

def convert_size(size_bytes):
   if size_bytes == 0:
       return "0B"
   size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
   i = int(math.floor(math.log(size_bytes, 1024)))
   p = math.pow(1024, i)
   s = round(size_bytes / p, 2)
   return "%s %s" % (s, size_name[i])

@Bot.on_message(filters.command("speedtest") & filters.user(ADMINS))
async def run_speedtest(client: Bot, message: Message):
    m = await message.reply_text("⚡️ Running Server Speedtest")
    try:
        test = speedtest.Speedtest()
        test.get_best_server()
        m = await m.edit("⚡️ Running Download Speedtest..")
        test.download()
        m = await m.edit("⚡️ Running Upload Speedtest...")
        test.upload()
        test.results.share()
        result = test.results.dict()
        print(result)
    except Exception as e:
        await m.edit(e)
        return
    m = await m.edit("🔄 Sharing Speedtest Results")
    output = f"""💡 <b>SpeedTest Results</b>
    
<u><b>⚡️Speed:</b></u>
<b>Ping:</b> {round(result['ping'])} ms
<b>Download:</b> {convert_size(result['download'])}
<b>Upload:</b> {convert_size(result['upload'])}

<u><b>Client:</b></u>
<b>ISP:</b> {result['client']['isp']}
<b>Country:</b> {result['client']['country']} 

<u><b>Server:</b></u>
<b>Name:</b> {result['server']['name']}
<b>Country:</b> {result['server']['country']}, {result['server']['cc']}
<b>Sponsor:</b> {result['server']['sponsor']}"""
    msg = await client.send_message(
        chat_id=message.chat.id, text=output
    )
    await m.delete()
