import pyromod.listen
import sys,os
import asyncio
from pyrogram import Client

from config import (
    API_HASH,
    APP_ID,
    LOGGER,
    TG_BOT_TOKEN2,
)
class Boty(Client):
    def __init__(self):
        super().__init__(
            "BotLee",
            api_hash=API_HASH,
            api_id=APP_ID,
            plugins={"root": "pluginsboty"},
            bot_token=TG_BOT_TOKEN2,
        )

        self.LOGGER = LOGGER
   
    async def start(self):
        try:
            await super().start()
            usr_bot_me = await super().get_me()
            self.username = usr_bot_me.username
            self.namebot = usr_bot_me.first_name
            self.LOGGER(__name__).info(
                f"Boty detected!\n┌ First Name: {self.namebot}\n└ Username: @{self.username}\n——"
            )
        except Exception as a:
            self.LOGGER(__name__).warning(a)
            self.LOGGER(__name__).info(
                "Bot2 Berhenti"
            )
            sys.exit()
