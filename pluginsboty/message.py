from pyrogram import filters,enums
from pyrogram.errors import FloodWait
from pyrogram.types import CallbackQuery, Message
import asyncio
from bot import Bot

from boty import Boty
from main import boty
from config import (
    FORCE_SUB_GROUP,
    FWBOT,
    BOTY
)
GC = FORCE_SUB_GROUP if FORCE_SUB_GROUP else "tikusnakaldisc"
@Boty.on_message(filters.chat(GC) & (filters.video | filters.document | filters.animation | filters.photo))
async def lee_onMessage(client, message):
    if message.from_user:
        a = await client.get_chat_member(FORCE_SUB_GROUP, message.from_user.id)
        if a.status == enums.ChatMemberStatus.MEMBER:
            await message.forward(GC)
            await message.delete()
@Boty.on_message(filters.private & ~filters.command(['start']))
async def lee_noAdmin(client, message):
    if message.from_user.id == FWBOT:
        if message.reply_to_message:
            iduser = 0
            if message.reply_to_message.forward_from:
                iduser = message.reply_to_message.forward_from.id
            else:
                toser = None
                if message.reply_to_message.caption:
                    try:
                        user,_ = message.reply_to_message.caption.split("=FWBOT",1)
                        toser = user
                    except Exception as e:
                        print(e)
                elif message.reply_to_message.text:
                    try:
                        user,_ = message.reply_to_message.text.split("=FWBOT",1)
                        toser = user
                    except Exception as e:
                        print(e)
                if toser is not None:
                   iduser = int(toser)

            caps = ""
            if message.caption:
                caps = message.caption
            match message.media:
                case enums.MessageMediaType.PHOTO:
                    await client.send_photo(iduser,message.photo.file_id,caps)
                case enums.MessageMediaType.AUDIO:
                    await client.send_audio(iduser,message.audio.file_id,caps)
                case enums.MessageMediaType.VIDEO:
                    await client.send_video(iduser,message.video.file_id,caps)
                case enums.MessageMediaType.DOCUMENT:
                    await client.send_document(iduser,message.document.file_id,caption=caps)
                case enums.MessageMediaType.ANIMATION:
                        await client.send_animation(iduser,message.animation.file_id,caption=caps)
                case enums.MessageMediaType.STICKER:
                    await client.send_sticker(iduser,message.sticker.file_id)
                case enums.MessageMediaType.VIDEO_NOTE:
                    await client.send_video_note(iduser,message.video_note.file_id)
                case enums.MessageMediaType.CONTACT:
                    await message.forward(iduser)
                case enums.MessageMediaType.VOICE:
                    await client.send_voice(iduser,message.voice.file_id,caps)
            if message.text and not message.media:
                await client.send_message(chat_id=iduser,text=message.text,reply_to_message_id=message.reply_to_message_id)

    else:
        fw = await message.forward(FWBOT)
        if not fw.forward_from:
            await fw.delete()
            txt = ""
            if message.text:
                txt = f"{message.from_user.id}=FWBOT\n{message.from_user.first_name}\n\n{message.text}"
                await client.send_message(
                    chat_id=FWBOT,
                    text=txt
                )
            elif message.media:
                cap = f"{message.from_user.id}=FWBOT\n{message.from_user.first_name}"
                if message.caption:
                    cap = f"{message.from_user.id}=FWBOT\n{message.from_user.first_name}\n\n{message.caption}"
                match message.media:
                    case enums.MessageMediaType.PHOTO:
                        await client.send_photo(FWBOT,message.photo.file_id,cap)
                    case enums.MessageMediaType.AUDIO:
                        await client.send_audio(FWBOT,message.audio.file_id,cap)
                    case enums.MessageMediaType.VIDEO:
                        await client.send_video(FWBOT,message.video.file_id,cap)
                    case enums.MessageMediaType.DOCUMENT:
                        await client.send_document(FWBOT,message.document.file_id,caption=cap)
                    case enums.MessageMediaType.ANIMATION:
                            await client.send_animation(FWBOT,message.animation.file_id,caption=cap)
                    case enums.MessageMediaType.STICKER:
                        await client.send_sticker(FWBOT,message.sticker.file_id)
                    case enums.MessageMediaType.VIDEO_NOTE:
                        await client.send_video_note(FWBOT,message.video_note.file_id)
                    case enums.MessageMediaType.CONTACT:
                        await message.forward(FWBOT)
                    case enums.MessageMediaType.VOICE:
                        await client.send_voice(FWBOT,message.voice.file_id,cap)



           


