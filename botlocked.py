import pyromod.listen
import sys,os
import asyncio
from pyrogram import Client

from config import (
    API_HASH,
    APP_ID,
    LOGGER,
    TG_BOT_LOCK,
)
class BotLock(Client):
    def __init__(self):
        super().__init__(
            "BotLock",
            api_hash=API_HASH,
            api_id=APP_ID,
            plugins={"root": "pluginslock"},
            bot_token=TG_BOT_LOCK,
        )

        self.LOGGER = LOGGER
        self.groupid = -1001680306954
   
    async def start(self):
        try:
            await super().start()
            usr_bot_me = await super().get_me()
            self.username = usr_bot_me.username
            self.namebot = usr_bot_me.first_name
            self.LOGGER(__name__).info(
                f"BotLocked detected!\n┌ First Name: {self.namebot}\n└ Username: @{self.username}\n——"
            )
        except Exception as a:
            self.LOGGER(__name__).warning(a)
            self.LOGGER(__name__).info(
                "BotLocked Berhenti"
            )
            sys.exit()
        try:
            info = await self.get_chat(self.groupid)
            link = info.invite_link
            _,link2 = link.split("/+",1)
            if not link:
                await self.export_chat_invite_link(self.groupid)
                link = info.invite_link
            self.invitelink = link
            self.invitekey = link2
        except Exception as a:
            self.LOGGER(__name__).warning(a)
            self.LOGGER(__name__).warning(
                "BotLock tidak dapat Mengambil link invite dari gc!"
            )
            self.LOGGER(__name__).warning(
                f"Pastikan @{self.username} adalah admin di Channel Tersebut, Chat ID F-Subs Channel Saat Ini: {FORCE_SUB_CHANNEL}"
            )
            self.LOGGER(__name__).info(
                "BotLock Berhenti. Gabung Group https://t.me/SharingUserbot untuk Bantuan"
            )
            sys.exit()
